<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="SalesOrder " language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="750" leftMargin="46" rightMargin="46" topMargin="14" bottomMargin="14" uuid="f1c2f9fb-e7c5-4271-ba2b-1ac4222611ab">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="from_date" class="java.lang.String">
		<parameterDescription><![CDATA[]]></parameterDescription>
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="to_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="from_so_no" class="java.lang.Long">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="to_so_no" class="java.lang.Long">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<parameter name="store_name" class="java.lang.Long">
		<defaultValueExpression><![CDATA[null]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
  a.code AS so_no,
  a.so_date,
  a.customer_name AS supplier_name,
  a.discount_amount AS discount,
  a.total_amount AS so_amount,
  b.caption AS org,
  c.caption AS address,
  d.caption AS store
FROM sls_so_mst a
 inner join  stp_branch_org b ON b.id=a.organization_id
 inner join  stp_address c ON c.id=b.address_id
 inner join  stp_business_unit d ON d.id=a.inv_store_id
where ($P{from_date} IS NULL or $P{to_date} is null or a.so_date   between to_date($P{from_date} ,'DD/MM/YYYY') AND to_date($P{to_date} ,'DD/MM/YYYY'))
AND( $P{from_so_no} IS NULL or  $P{to_so_no} is null or a.id between $P{from_so_no} AND $P{to_so_no})
AND( $P{store_name} IS NULL or d.id=$P{store_name})
;]]>
	</queryString>
	<field name="so_no" class="java.lang.String"/>
	<field name="so_date" class="java.sql.Timestamp"/>
	<field name="supplier_name" class="java.lang.String"/>
	<field name="discount" class="java.lang.Float"/>
	<field name="so_amount" class="java.lang.Float"/>
	<field name="org" class="java.lang.String"/>
	<field name="address" class="java.lang.String"/>
	<field name="store" class="java.lang.String"/>
	<variable name="discount_sum" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{discount}]]></variableExpression>
	</variable>
	<variable name="amount_sum" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{so_amount}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="79" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="0" width="750" height="20" uuid="8fa6e1ab-f2bc-4c94-8115-e52c93db51a6"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{org}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="20" width="750" height="20" uuid="9013b66e-d484-47b0-8066-8cf737bf9277"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{address}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="291" y="40" width="166" height="19" uuid="3ce236cb-2f96-406f-a6fc-1a74f2485ad0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[SO Summary Report]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="59" width="750" height="20" uuid="1f5d31a8-3748-4a89-ac4f-06b701953f21"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($P{from_date}==null? "":$P{from_date})+  " "  +($P{to_date}==null?"":"to "+$P{to_date})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="30" height="20" uuid="09355b3e-9f19-4035-9c32-6d48f63fe236"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[SL]]></text>
			</staticText>
			<staticText>
				<reportElement x="30" y="0" width="106" height="20" uuid="6429e098-63f0-4809-ba43-6462c2d0ee2d"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[SO No]]></text>
			</staticText>
			<staticText>
				<reportElement x="136" y="0" width="100" height="20" uuid="b7c7fefc-a4ba-4441-a061-2726c6d7bb00"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="236" y="0" width="221" height="20" uuid="ec073689-78dc-44db-841b-4c07cdfd2b9e"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Supplier Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="457" y="0" width="122" height="20" uuid="5712a5a6-26a8-466b-8ce9-6d7616a4bddc"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Store]]></text>
			</staticText>
			<staticText>
				<reportElement x="579" y="0" width="71" height="20" uuid="65a8d537-28cc-4fb9-80de-c21d431e27cf"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Discount]]></text>
			</staticText>
			<staticText>
				<reportElement x="650" y="0" width="100" height="20" uuid="9952dc8d-8f0a-4fe0-aa50-6fe56a8bdf6e"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[SO Amount]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="0" width="30" height="20" uuid="dbcc8d2a-9308-4f62-8550-bf1fba8ffc0a"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{COLUMN_COUNT}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="30" y="0" width="106" height="20" uuid="ab1ed641-9702-48ca-9c31-52b16badce55"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{so_no}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="136" y="0" width="100" height="20" uuid="e91dc0f5-1183-49bc-aa4b-57475f5ca24e"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{so_date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="236" y="0" width="221" height="20" uuid="c6f213b3-1634-4f53-994c-a318efda6839"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{supplier_name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="457" y="0" width="122" height="20" uuid="9be57f22-cd71-46a9-a596-e1a7da928bae"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{store}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="579" y="0" width="71" height="20" uuid="8d2bcf4c-74e9-41e7-92c2-9255f7e28374"/>
				<box leftPadding="0" rightPadding="3">
					<pen lineWidth="0.25" lineColor="#CCCCCC"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{discount}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="650" y="0" width="100" height="20" uuid="bc9f963b-ba72-41f1-9fe7-5d575cebf7e9"/>
				<box leftPadding="0" rightPadding="3">
					<pen lineWidth="0.25" lineColor="#CCCCCC"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{so_amount}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="579" height="20" uuid="6581d835-be8d-49db-9a53-1ef163f9de9c"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="579" y="0" width="71" height="20" uuid="e56d4565-4816-4912-8500-9162aa4ad946"/>
				<box leftPadding="0" rightPadding="3">
					<pen lineWidth="0.25" lineColor="#CCCCCC"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{discount_sum}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern=" #,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="650" y="0" width="100" height="20" uuid="384aced9-e350-4eb5-95fa-380c76198aa1"/>
				<box leftPadding="0" rightPadding="3">
					<pen lineWidth="0.25" lineColor="#CCCCCC"/>
					<topPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<leftPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<bottomPen lineWidth="0.25" lineColor="#CCCCCC"/>
					<rightPen lineWidth="0.25" lineColor="#CCCCCC"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{amount_sum}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
