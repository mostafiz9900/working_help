<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="refresh" content="5">
		<title>Welcome to Grails</title>

	</head>
	<body>
	<div id="page-body" role="main">
			<h1>WELCOME PAGE</h1>

			<div id="controller-list" role="navigation">
				<h2>Available Controllers:</h2>
				<ul>
					<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
						<li class="controller"><g:link controller="${c.logicalPropertyName}">${c.getName()}</g:link></li>
					</g:each>
				</ul>
				<ul>
					<sec:ifLoggedIn>
						<a href="${g.createLink(controller: 'logout')}">
							<i class="fa fa-sign-out"></i> Log out
						</a>
					</sec:ifLoggedIn>
					<sec:ifNotLoggedIn>
						<a href="${g.createLink(controller: 'user', action: 'index')}">
							<i class="fa fa-sign-in"></i> Log In
						</a>
					</sec:ifNotLoggedIn>

				</ul>
			</div>
		</div>
	</body>
</html>
